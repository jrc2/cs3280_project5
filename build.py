import os
from pybuilder.core import use_plugin
from pybuilder.core import init
from pybuilder.core import Author
from pybuilder.core import task
from pybuilder.core import use_bldsup
use_bldsup(build_support_dir=os.path.join('src', 'main', 'python'))
import portscan

# plugins for common tasks
use_plugin('python.core')
use_plugin('python.unittest')
use_plugin('python.install_dependencies')
use_plugin('python.distutils')

default_task = "publish"


@init
def initialize(project):
    project.name = 'Multi-process Portscan'
    project.version = '1.0'
    project.summary = 'Project for CS3280'
    project.description = '''Scans ports concurrently'''
    project.authors = [Author('John Chittam', 'jchitta2@my.westga.edu')]
    project.license = "not for redistribution"
    project.url = 'https://cs.westga.edu'


@task
def scan_port_60000():
    print(portscan.scan('localhost', 60000))


@task
def scan_port_60000_to_60010():
    print(portscan.scan('localhost', 60000, 60010))
