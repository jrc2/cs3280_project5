import unittest
import socket
import portscan


class PortscanTests(unittest.TestCase):

    def testSingleOpenPort(self):
        sock60000 = socket.socket()
        sock60000.bind(('localhost', 60000))
        sock60000.listen(1)

        portData = portscan.scan('localhost', 60000)

        sock60000.close()

        self.assertEqual(1, len(portData))
        self.assertEqual(True, portData[60000])


    def testSameStartAndEndPort(self):
        sock60010 = socket.socket()
        sock60010.bind(('localhost', 60010))
        sock60010.listen(1)

        portData = portscan.scan('localhost', 60010, 60010)

        sock60010.close()

        self.assertEqual(1, len(portData))
        self.assertEqual(True, portData[60010])


    def testEndPortIsNone(self):
        sock60011 = socket.socket()
        sock60011.bind(('localhost', 60011))
        sock60011.listen(1)

        portData = portscan.scan('localhost', 60011, None)

        sock60011.close()

        self.assertEqual(1, len(portData))
        self.assertEqual(True, portData[60011])


    def testSingleClosedPort(self):
        portData = portscan.scan('localhost', 60000)
        self.assertEqual(1, len(portData))
        self.assertEqual(False, portData[60000])


    def testMultiplePortsAllOpen(self):
        s60001 = socket.socket()
        s60001.bind(('localhost', 60001))
        s60001.listen(1)
        s60002 = socket.socket()
        s60002.bind(('localhost', 60002))
        s60002.listen(1)
        s60003 = socket.socket()
        s60003.bind(('localhost', 60003))
        s60003.listen(1)

        portData = portscan.scan('localhost', 60001, 60003)

        s60001.close()
        s60002.close()
        s60003.close()

        self.assertEqual(3, len(portData))
        self.assertEqual(True, portData[60001])
        self.assertEqual(True, portData[60002])
        self.assertEqual(True, portData[60003])


    def testMultiplePortsAllClosed(self):
        portData = portscan.scan('localhost', 50000, 50002)

        self.assertEqual(3, len(portData))
        self.assertEqual(False, portData[50000])
        self.assertEqual(False, portData[50001])
        self.assertEqual(False, portData[50002])


    def testMultiplePortsSomeOpenSomeClosed(self):
        s60004 = socket.socket()
        s60004.bind(('localhost', 60004))
        s60004.listen(1)
        s60006 = socket.socket()
        s60006.bind(('localhost', 60006))
        s60006.listen(1)

        portData = portscan.scan('localhost', 60004, 60006)

        s60004.close()
        s60006.close()

        self.assertEqual(3, len(portData))
        self.assertEqual(True, portData[60004])
        self.assertEqual(False, portData[60005])
        self.assertEqual(True, portData[60006])


    def testEndPortSmallerThanStartPort(self):
        self.assertRaises(ValueError, portscan.scan, 'localhost', 60004, 60000)


    def testIpIsNone(self):
        self.assertRaises(ValueError, portscan.scan, None, 60004, 60005)


    def testStartPortIsNone(self):
        self.assertRaises(ValueError, portscan.scan, 'localhost', None, 60005)
