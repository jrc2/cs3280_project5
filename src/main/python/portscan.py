import sys
import socket
from multiprocessing import Process, Pipe


def scan(ip, startPort, endPort=None):
    if ip is None:
        raise ValueError('you must input IP address')
    if startPort is None:
        raise ValueError('you must supply starting port')
    if endPort is None:
        endPort = startPort
    elif int(endPort) < int(startPort):
        raise ValueError('end port cannot be less than starting port')

    parent_conn, child_conn = Pipe()

    for port in range(int(startPort), int(endPort) + 1):
        process = Process(target=scanSinglePort, args=(ip, port, child_conn))
        process.start()

    portData = {}

    while True:
        portData.update(parent_conn.recv())
        if len(portData) == int(endPort) + 1 - int(startPort):
            return portData


def scanSinglePort(ip, port, pipe):
    if ip is None:
        raise ValueError('you must input IP address')
    if port is None:
        raise ValueError('you must supply port')

    aSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        aSocket.connect((ip, int(port)))
        pipe.send({port: True})
    except ConnectionRefusedError:
        pipe.send({port: False})


if __name__ == '__main__':
    ipAddress = sys.argv[1]
    startPort = sys.argv[2]
    if len(sys.argv) == 4:
        endPort = sys.argv[3]
    else:
        endPort = None

    print(scan(ipAddress, startPort, endPort))
